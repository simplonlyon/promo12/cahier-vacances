# Cahier de Vacances

Voici quelques petits exercices sur des thèmes variables à faire si vous le souhaitez pendant vos vacances (ou votre temps libre) pour ne pas perdre la main ou consolider vos acquis.

La solution des exercices est sur la branche solution de ce même dépôt sauf si indiqué autrement dans la consigne.

## [DOM](./dom)
## [Récursivité](./recursivite)
## [Refactorisation](./refactorisation)
## Grid Cheatsheet Selectable
En partant de [cette exercice](https://gitlab.com/simplonlyon/promo12/js-dom/blob/master/js/grid-cheatsheet.js), rajouter la possibilité de
sélectionner les colonnes pour modifier leur col-x individuellement (et donc probablement supprimer la modification des col-x globales). La solution sera sur le dépôt js-dom sur une branche solution.

Bonus Compliqué (et non corrigé) : Faire que quand on sélectionne une colonne, la valeur de l'input change pour prendre le nombre de colonnes que fait la colonne sélectionnée. (genre si je clique sur une col-3 la valeur de l'input passe à 3)

<details><summary>Indice/Etapes</summary>
<p>

1. Créer une variable selectedCol initialisée à null
2. Dans la boucle initial qui ajoute le 'col-4' sur toutes les col, rajouter également une event au click sur chaque col qui mettra la col en question dans la variable selectedCol
3. Modifier l'event sur le colInput pour y dégager la boucle et faire en sorte que l'assignation de la classe se fasse sur la variable selectedCol
4. Rajouter un if dans l'event pour faire qu'il ne fasse l'assignation de la classe que si selectedCol n'est pas null
5. Faire en sorte de display: none le input-col par défaut dans le javascript puis modifier l'event au click sur chaque col pour faire qu'en plus d'assigner à la variable selectedCol, cela display: block l'input (comme ça il ne sera affiché que si on a une colonne sélectionnée)

</p>
</details>

## Guess a number v2
Refaire l'exercice consistant à faire deviner un nombre qu'on avait l'autre jour avec des prompt ([lui là](https://gitlab.com/simplonlyon/promo12/js-introduction/blob/master/js/loop.js)), mais cette fois ci utiliser le html pour les interactions utilisateur.ice (solution dans les fichiers guess.js et guess.html sur ce dépôt, branch solution)

<details><summary>Indice</summary>
<p>

* Déjà on peut dégager la boucle car maintenant ça se fera via les event
* On pourra avoir un input pour entrer notre guess, un button pour valider notre guess et un paragraphe pour y afficher la réponse
* Ce qui était dans la boucle sera maintenant dans l'event click du button

</p>
</details>


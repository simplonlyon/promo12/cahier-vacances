La refactorisation consiste à prendre un peu de recul sur son code et le modifier pour réduire les répétitions, le rendre plus maintenable et plus optimisé. Cela passe souvent par la création de fonctions et ou l'identification des parties qui pourraient varier dans une fonction.

## Exercices 1, 2 et 3
Refactoriser le code contenu dans les fichiers JS
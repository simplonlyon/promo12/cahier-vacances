# Exercices sur le DOM

## Exercice 1 : Faire un compteur de caractères
Créer un champ de textes et faire en sorte qu'au fur et à mesure qu'on tape dans le champ, ça nous affiche le nombre de caractères. Bonus, rajouter une limite de caractères, genre 150.

<details><summary>Indice</summary>
<p>

* On peut créer le textarea et le p (pour les caractères) directement dans le html
* Il va falloir rajouter un event au keyup par exemple sur le textarea
* Il va falloir choper le contenu du textarea pour vérifier sa taille et l'assigner au p
* Pour le bonus, il faudra faire en sorte de racourcir le contenu du textarea à chaque keyup

</p>
</details>

## Exercice 2 : Faire un glisser-déposer
Faire en sorte que lorsqu'on a le bouton de la souris enfoncé sur le carré, celui ci se déplace en même temps que la souris

<details><summary>Indice</summary>
<p>

* Il va falloir rajouter un event mousedown et utiliser le paramètre event pour récupérer la position de la souris
* Il faut vérifier si le bouton de la souris est actuellement enfoncé ou non
* Il va falloir plus d'un seul event

Indices assez détaillé :

Moins logique qu'il n'y paraît, pour avoir un résultat satisfaisant il faudra créer une variable booléenne indiquant si le bouton est enfoncé ou pas, cette variable changera de valeur au mousedown sur le carré et repassera à false au mouseup sur `window`.
Ensuite il faudra rajouter un event mousemove sur `window` également, y récupéré le clientX et clientY dans le paramètre event et assigner ces valeurs au top et au left du carré.

</p>
</details>
/**
 * exemple de fonction récursive qui affiche 10 fois une phrase
 */

 //La fonction prend le compte actuel en argument (par défaut, 0)
 function recursiveLoop(count = 0) {
    //affichage de la phrase
   console.log("bloup");
   //incrémentation du compte
   count++;
   //Si le compte est inférieur à 10...
   if(count < 10) {
       //...on relance la même fonction en lui donnant le compte incrémenté de 1
       recursiveLoop(count);
   }
}



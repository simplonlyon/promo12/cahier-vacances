La récursivité désigne le fait d'avoir par exemple une fonction qui s'appelle elle même sous certaines conditions.

C'est une notion assez puissante de la programmation qui permet de faire certaines choses similaires à des boucles.
## Exercice 1
Faire une fonction qui attend un tableau en argument et qui parcours ce tableau via de la récursivité. (exemple dans le fichier js)
<details><summary>Indice</summary>
<p>
La fonction sera presqu'identique à l'exemple au dessus il faudra simplement avoir un argument en plus (le tableau) et modifier la condition pour faire que la boucle ne soit relancer que si le compte est inférieur à la taille du tableau.
</p>
</details>

La récursivité pourra être utilisée pour parcourir des "choses" pouvant avoir plusieurs dimension, par exemple, un tableau contenant d'autres tableaux.

## Exercice 2
Faire une fonction qui fait une boucle sur un tableau qui :
* affichera la valeur si c'est une valeur simple (genre un string ou un number)
* se relancera elle même si la valeur actuelle est un tableau afin de parcourir le tableau de manière récursive

<details><summary>Indice</summary>
<p>

* La fonction attendra un seul argument, le tableau à parcourir
* La première étape est de faire une boucle sur la tableau dans la fonction
* Ensuite faire une condition sur la valeur actuelle pour savoir si c'est un tableau ou pas avec is_array
* Si ce n'est pas un tableau, on fait juste un echo de la valeur
* Si c'est un tableau, on relance la même fonction mais en lui donnant en argument la valeur actuelle

</p>
</details>

## Exercice 3
En utilisant le principe de la récursivité, créer une fonction qui prendra en argument un élément HTML (capturé avec querySelector) et qui affichera en console le innerText et le nodeName de chaque élément html contenus dans celui ci de façon récursive.

Exemple de résultat attendu si on donne le `main` du fichier exercice-03.html à la fonction : 
```
H1 ---- Ma page
SECTION ---- Première Section

Du texte pour l'exemple avec un span

Plus de texte
H2 ---- Première Section
P ---- Du texte pour l'exemple avec un span
```